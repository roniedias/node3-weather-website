const path = require('path')
const express = require('express')
const hbs = require('hbs')
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const app = express()
const port = process.env.PORT || 3000

// Define paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(publicDirectoryPath))

app.get('', (req, resp) => {
    resp.render('index', {
        title: 'Weather',
        name: 'Ronie Dias'
    })
})

app.get('/about', (req, resp) => {
    resp.render('about', {
        title: 'About me',
        name: 'Ronie Dias'
    })
})

app.get('/help', (req, resp) => {
    resp.render('help', {
        title: 'Help',
        name: 'Ronie Dias'
    })
})


app.get('/weather', (req, res) => {

    geocode(req.query.address, (error, {latitude, longitude, location} = {}) => {

        if(!req.query.address) {
            return res.send({
                error: 'You must provide an address'
            })
        }
        
        if(error) {
            return res.send({ error})
        }
    
        forecast(latitude, longitude, (error, forecast) => {
    
            if(error) {
                return res.send({ error })
            }
    
            res.send({
                forecast,
                location,
                address: req.query.address
            })
            
        })    
    })

})

app.get('/help/*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Ronie Dias',
        errorMessage: 'Help article not found'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: '404',
        name: 'Ronie Dias',
        errorMessage: 'Page not found'
    })
})

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})